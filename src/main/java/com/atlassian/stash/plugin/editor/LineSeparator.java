package com.atlassian.stash.plugin.editor;

public enum LineSeparator {

    CR("\r"),
    LF("\n"),
    CRLF("\r\n");

    private final String separator;

    LineSeparator(String separator) {
        this.separator = separator;
    }

    public String getSeparator() {
        return separator;
    }

    public static LineSeparator resolve(String content, long sampleSize) {
        int cr = 0;
        int lf = 0;
        int crlf = 0;

        boolean prevWasCr = false;

        for (int i = 0; i < content.length() && i < sampleSize; i++) {
            char c = content.charAt(i);
            if (c == '\n') {
                if (prevWasCr) {
                    crlf++;
                } else {
                    lf++;
                }
            } else {
                if (prevWasCr) {
                    cr++;
                }
                prevWasCr = c == '\r';
            }
        }

        if (cr > lf && cr > crlf) {
            return CR;
        } else if (crlf > lf) {
            return CRLF;
        } else {
            return LF;
        }
    }

}
